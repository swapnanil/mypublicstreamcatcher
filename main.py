# Import the necessary package to process data in JSON format
try:
    import json
except ImportError:
    import simplejson as json

import time

# Import the necessary methods from "twitter" library
from twitter import Twitter, OAuth, TwitterHTTPError, TwitterStream

# Variables that contains the user credentials to access Twitter API 
ACCESS_TOKEN = '133326146-lbHZ7dbZndFOSg6tNoHm02kWWgpzJllua3gYkYlb'
ACCESS_SECRET = 'lUIQuuiHmw5YIvYrVAhAmEcdLsMpNDpFvzCms7Rh1wAr3'
CONSUMER_KEY = 'HyFx6eiGDi6W8grt79VYJPwr8'
CONSUMER_SECRET = 'CAbTM886revs1L9Df9nVUytwyKONY9w3d9Vub3zC1fqc6hTelM'

oauth = OAuth(ACCESS_TOKEN, ACCESS_SECRET, CONSUMER_KEY, CONSUMER_SECRET)

# Initiate the connection to Twitter Streaming API
twitter_stream = TwitterStream(auth=oauth)

# Get a sample of the public data following through Twitter
iterator = twitter_stream.statuses.filter(track="canada")

# Print each tweet in the stream to the screen 
# Here we set it to stop after getting 1000 tweets. 
# You don't have to set it to stop, but can continue running 
# the Twitter API to collect data for days or even longer. 
while 1 < 2:
    tweet_count = 1000
    for tweet in iterator:
        tweet_count -= 1
        # Twitter Python Tool wraps the data returned by Twitter
        # as a TwitterDictResponse object.
        # We convert it back to the JSON format to print/score

        # The command below will do pretty printing for JSON data, try it out
        # print json.dumps(tweet, indent=4)

        if tweet_count <= 0:
            filename = './data/' + str(int(time.time())) + '.json'
            with open(filename, 'w') as file:
                json.dump(tweet, file, indent=4)
            break
